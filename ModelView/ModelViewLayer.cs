﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using Mutable;


using Model; // n.b. THIS EXAMPLE DOES NOT PRACTICE GOOD MODULAR DESIGN
             // DO NOT EMULATE THIS AS AN EXAMPLE OF HOW TO IMPLEMENT MVVM
             // IN THIS REGARD. There's no reason that the modelview should
             // really have a dependency on the model, or that the view should
             // have a dependency on the model view.  Use loose coupling!

namespace ModelView
{
    public class ModelViewLayer
    {
        public ObservableCollection<Mutable<GeoLocation>> Locations
        {
            get;
            set;
        }

        public SaveCommand SaveAll
        {
            get;
            private set;
        }

        public SaveCommand SaveOnlyChanged
        {
            get;
            private set;
        }

        private ModelLayer mo_Model;

        public ModelViewLayer()
        {
            mo_Model = new ModelLayer();

            //This is the most important step.  We get from the model the
            //locations, and wrap them all in mutables, with a delegate that provides
            //a constructor.

            IEnumerable<Mutable<GeoLocation>> oMutableLocations =  from oLocation in mo_Model.GetLocations()
                                                                   select new Mutable<GeoLocation>(oLocation,
                                                                                                   GeoLocation.ConstructFromDictionary);

            this.Locations = new ObservableCollection<Mutable<GeoLocation>>(oMutableLocations);

            this.SaveAll = new SaveCommand(PerformSaveAll);

            this.SaveOnlyChanged = new SaveCommand(PerformSaveChanged);
        }

        private void PerformSaveAll()
        {
            mo_Model.SaveLocations(from oMutable in Locations
                                   select oMutable.ProduceImmutable());
        }

        private void PerformSaveChanged()
        {
            mo_Model.SaveLocations(from oMutable in Locations
                                   where oMutable.IsChanged
                                   select oMutable.ProduceImmutable());
        }
    }

    public class SaveCommand : ICommand
    {
        private Action mo_RunMe;

        public SaveCommand(Action RunMe)
        {
            this.mo_RunMe = RunMe;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            if (mo_RunMe != null)
            {
                mo_RunMe();
            }
        }
    }

}
