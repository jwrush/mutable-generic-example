﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Dynamic;
using System.Reflection;

namespace Mutable
{
    public class Mutable<InnerType> : DynamicObject
    {
        private static IEnumerable<PropertyInfo> INNER_TYPE_PROPERTIES = null;
        private static object INNER_TYPE_PROPERTIES_LOCK = new object();

        private InnerType mo_Source;
        private Func<Dictionary<string, object>, InnerType> mo_Constructor;
        private Dictionary<string, object> mo_Changes;

        /// <summary>
        /// Gets a value indicating whether this instance is changed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is changed; otherwise, <c>false</c>.
        /// </value>
        public bool IsChanged
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Mutable&lt;InnerType&gt;"/> class, a wrapper
        /// for placing around immutable objects that are going to be bound to editable controls.
        /// </summary>
        /// <param name="Source">The source.</param>
        /// <param name="Constructor">A delegate that maps the names of the properties to their values and then calls the constructor for the underlying type </param>
        public Mutable(InnerType Source, Func<Dictionary<string, object>, InnerType> Constructor)
        {
            //once per InnerType, use reflection to get a list of the properties.
            lock (INNER_TYPE_PROPERTIES_LOCK)
            {
                if (INNER_TYPE_PROPERTIES == null)
                {
                    INNER_TYPE_PROPERTIES = typeof(InnerType).GetProperties();
                }
            }

            mo_Changes = new Dictionary<string,object>();
            IsChanged = false;

            //make a dictionary with the values from the readable properties on InnerType
            foreach (PropertyInfo oProp in INNER_TYPE_PROPERTIES)
            {
                if (oProp.CanRead)
                {
                    mo_Changes.Add(oProp.Name, oProp.GetValue(Source, null));
                }
            }

            mo_Source = Source;
            mo_Constructor = Constructor;
        }


        /// <summary>
        /// Records a change to a property
        /// </summary>
        /// <param name="PropertyName">Name of the property.</param>
        /// <param name="Value">The value.</param>
        public void RecordChange(string PropertyName, object Value)
        {
             mo_Changes[PropertyName] = Value;
             IsChanged = true;
        }

        /// <summary>
        /// Provides the implementation for operations that set member values. Classes derived from the <see cref="T:System.Dynamic.DynamicObject"/> class can override this method to specify dynamic behavior for operations such as setting a value for a property.
        /// </summary>
        /// <param name="binder">Provides information about the object that called the dynamic operation. The binder.Name property provides the name of the member to which the value is being assigned. For example, for the statement sampleObject.SampleProperty = "Test", where sampleObject is an instance of the class derived from the <see cref="T:System.Dynamic.DynamicObject"/> class, binder.Name returns "SampleProperty". The binder.IgnoreCase property specifies whether the member name is case-sensitive.</param>
        /// <param name="value">The value to set to the member. For example, for sampleObject.SampleProperty = "Test", where sampleObject is an instance of the class derived from the <see cref="T:System.Dynamic.DynamicObject"/> class, the <paramref name="value"/> is "Test".</param>
        /// <returns>
        /// true if the operation is successful; otherwise, false. If this method returns false, the run-time binder of the language determines the behavior. (In most cases, a language-specific run-time exception is thrown.)
        /// </returns>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            if (mo_Changes.ContainsKey(binder.Name))
            {
                RecordChange(binder.Name, value);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Provides the implementation for operations that get member values. Classes derived from the <see cref="T:System.Dynamic.DynamicObject"/> class can override this method to specify dynamic behavior for operations such as getting a value for a property.
        /// </summary>
        /// <param name="binder">Provides information about the object that called the dynamic operation. The binder.Name property provides the name of the member on which the dynamic operation is performed. For example, for the Console.WriteLine(sampleObject.SampleProperty) statement, where sampleObject is an instance of the class derived from the <see cref="T:System.Dynamic.DynamicObject"/> class, binder.Name returns "SampleProperty". The binder.IgnoreCase property specifies whether the member name is case-sensitive.</param>
        /// <param name="result">The result of the get operation. For example, if the method is called for a property, you can assign the property value to <paramref name="result"/>.</param>
        /// <returns>
        /// true if the operation is successful; otherwise, false. If this method returns false, the run-time binder of the language determines the behavior. (In most cases, a run-time exception is thrown.)
        /// </returns>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (this.mo_Changes.ContainsKey(binder.Name))
            {
                result = mo_Changes[binder.Name];
                return true;
            }
            else
            {
                result = null;
                return false;
            }
        }

        /// <summary>
        /// Produces a new immutable using either the source object's values for the properties, or, if they've been changed since this 
        /// object's construction, the new values that have been collected in the changes dictionary.  The construction is done using the 
        /// delegate we were given in the constructor.
        /// </summary>
        /// <returns></returns>
        public InnerType ProduceImmutable()
        {
            return mo_Constructor(mo_Changes);
        }
    }
}
