﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    //An immutable class representing location on the earth.
    public class GeoLocation
    {
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
        public double Altitude { get; private set; }

        public GeoLocation(double Latitude, double Longitude, double Altitude)
        {
            this.Latitude = Latitude;
            this.Longitude = Longitude;
            this.Altitude = Altitude;

            System.Diagnostics.Debug.Print("Constructing new GeoLocation {0}", this);
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}, {2}", Latitude, Longitude, Altitude);
        }

        //I put the methods that are used to construct the mutable versions of immutables in
        //the immutable classes that they construct so that they have a localized location.
        //This has downsides, as static methods can't be easily unit tested, but I think it gives
        //us more than it takes away.
        public static GeoLocation ConstructFromDictionary(Dictionary<string, object> DictionaryOfArgs)
        {
            return new GeoLocation(Convert.ToDouble(DictionaryOfArgs["Latitude"]),
                                   Convert.ToDouble(DictionaryOfArgs["Longitude"]),
                                   Convert.ToDouble(DictionaryOfArgs["Altitude"]));
        }
    }
}
