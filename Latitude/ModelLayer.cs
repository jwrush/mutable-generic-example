﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class ModelLayer
    {
        public IEnumerable<GeoLocation> GetLocations()
        {
            return new GeoLocation[]{ new GeoLocation(12.0, 60.0, 1000.00),
                                      new GeoLocation(14.0, 42.0, 999.00) };
        }

        public void SaveLocations(IEnumerable<GeoLocation> ToSave)
        {
            foreach (var oItem in ToSave)
            {
                System.Diagnostics.Debug.Print("Saving {0}", oItem);
            }
        }
    }
}
